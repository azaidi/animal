import argparse
import users

parser = argparse.ArgumentParser(description="Find one or many users.")

# Add the arguments
parser.add_argument('-n','--name',
                       type=str,
                       help='Search by name')

parser.add_argument('-e','--exact',
                       type=bool,
                       help='Search exactly for this name')

parser.add_argument('-f','--format',
                       type=str,
                       help='Return in the one of the supported formats.')

# Execute the parse_args() method
args = parser.parse_args()
collection = users.fetch(args.name or None, args.name and args.exact)

if args.format:
    print collection.get(args.format)
else:
    print collection
