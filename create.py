import argparse
import users

parser = argparse.ArgumentParser(description="Create a new user")

# Add the arguments
parser.add_argument('name', type=str, help='Full name')

parser.add_argument('-ad', '--address', type=str, help='A street address')

parser.add_argument('-ph', '--phone', type=str, help='A phone number (as a string)')

# Execute the parse_args() method
args = parser.parse_args()
existing = users.fetch_first(args.name)

if existing:
    raise Exception("A user with this name already exists, try another name.")

print "Creating new user", args.name
new_user = users.User(name=args.name, address=args.address, phone=args.phone)
print "saving...."
status = new_user.save()
print status
