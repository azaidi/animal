import json
import yaml
import os
import glob
import tempfile
import webbrowser

# path to the /db folder where user json files are stored
DB_PATH = os.path.join(os.path.dirname(__file__), "db")

# html base template to inject user results into
HTML_TEMPLATE = """
<!DOCTYPE html>
<html>
<body>
{0}
</body>
</html>
"""


def fetch(name=None, exact=False):
    """
    Fetch users based on files saved in the /db folder. The default search pattern will return all files.
    If a name is entered it will return matching results

    :param str name: A name to search for
    :param bool exact: If you want an exact search without similar names then turn this to True
    :rtype: UserCollection
    :return: A collection of users

    Test for 'homer simpson', included in db:
    >>> fetch(name="homer simpson", exact=True)
    [Homer Simpson]

    """
    search_like = "*"
    user_files = []

    if name:

        pattern = "*{0}*.json"
        safe_name = name.lower().strip().replace(" ", "_")

        if exact:
            pattern = "{0}.json"

        search_like = pattern.format(safe_name)

    # use glob to search '/db/' directory for matching .json files
    for path in glob.glob(os.path.join(DB_PATH, search_like)):
        user = UserRecord(path)
        user_files.append(user)

    collection = UserCollection(user_files)

    return collection


def fetch_first(name):
    """
    Returns only 1 result based on an exact name search.
    :param str name: The exact name to search for
    :rtype: UserRecord
    :return: a user record

    >>> fetch_first('homer simpson')
    Homer Simpson
    """
    collection = fetch(name, exact=True)
    if collection.user_files:
        return collection[0]

    return None


class UserCollection(object):
    """
    A collection of users that can be displayed in different formats

    Test if collection can initialize and load requested data
    >>> some_files = [
    ...                 UserRecord(os.path.join(DB_PATH, "homer_simpson.json")),
    ...                 UserRecord(os.path.join(DB_PATH, "asim_zaidi.json"))
    ...               ]
    >>> coll = UserCollection(some_files)
    >>> coll
    [Homer Simpson, Asim Zaidi]
    >>> coll.load()
    True

    Can we get a list back?
    >>> type(coll.get("list")) == list
    True

    """
    def __init__(self, user_files):
        """
        :param list user_files: a list of UserRecord instances
        """
        self.user_files = user_files
        self.users = []

        # track state of collection being populated with user data from file
        self.loaded = False

        # mapping of supported formats and their callable methods, register new formats here
        self.supported_outputs = {
            "list": self.to_list,
            "json": self.to_json,
            "yaml": self.to_yaml,
            "text": self.to_text,
            "html": self.to_html
        }

        # file extentions to save the format in
        self.supported_extensions = {
            "list": ".py",
            "json": ".json",
            "yaml": ".yaml",
            "text": ".txt",
            "html": ".html"
        }

    def __repr__(self):
        return repr(self.user_files)

    def __getitem__(self, item):
        return self.user_files[item]

    def get_users(self):
        """
        Forces all data to be loaded from the file database into the User instance
        :return: bool
        """
        for user_file in self.user_files:
            user = user_file.user
            if user:
                self.users.append(user)

        self.loaded = True

        return self.users

    @property
    def formats(self):
        """List of supported output formats"""
        return self.supported_outputs.keys()

    def get(self, output="list"):
        """Convert to a supported output type"""
        if output not in self.supported_outputs.keys():
            raise Exception("Output format %s is not supported." % output)

        call_this = self.supported_outputs[output]

        return call_this()

    def browse(self, output="html"):
        """Saves to temp and opens the requested format using webbrowser module"""
        if output == "list":
            raise Exception("List format is not supported by this method.")

        data = self.get(output)
        _, temp = tempfile.mkstemp(self.supported_extensions[output])
        f = open(temp, "w+")
        f.write(data)
        f.close()
        webbrowser.open(temp)

    def load(self):
        """
        Make sure we have loaded data for all files just once. This is for speed so we only need
        to access the file system once and not repeatedly. Once loaded, we can convert between different formats.
        """
        if self.loaded is False:
            self.get_users()

        return True

    def to_list(self):
        """
        Get a list of users represented by their raw data type
        :return: list
        """
        self.load()
        return [user.data for user in self.users]

    def to_json(self):
        """
        Get a json formatted string for this collection.
        :return: str
        """
        return json.dumps(self.to_list(), indent=4)

    def to_yaml(self):
        """
        Get a json formatted string for this collection.
        :return: str
        """
        return yaml.dump(self.to_list())

    def to_text(self):
        """
        Get a text based representation of this collection.
        :return: str
        """
        self.load()
        blob = ""
        for user in self.users:
            for key in user.data.keys():
                blob += key + ": " + str(user.data[key]) + ", "
            blob += "\n"

        return blob

    def to_html(self):
        """
        Get a html table based representation of this collection.
        :return: str
        """
        self.load()

        blob = '<table style="width:100%">'
        blob += '<th>Name</th>'
        blob += '<th>Phone</th>'
        blob += '<th>Address</th>'

        for user in self.users:
            blob += "<tr>"
            for key in ["name","phone","address"]:
                blob += "<td>" + str(user.data.get(key, "---")) + "</td>"
            blob += "<tr>"
        blob += "</table>"

        html_blob = HTML_TEMPLATE.format(blob)
        return html_blob


class UserRecord(object):
    """
    This class helps represent a user file path as a readable record from a
    file based database without accessing any file contents. This helps with returning search results as readable names.
    The actual record contents can be loaded by accessing the `user` property.

    Test if UserRecord can initialize
    >>> UserRecord(os.path.join(DB_PATH, "homer_simpson.json"))
    Homer Simpson
    """
    def __init__(self, path):
        self.path = path

    def __repr__(self):
        file_name = os.path.basename(self.path)

        # remove the file extention
        no_ext = os.path.splitext(file_name)[0]

        # get the user's name and capitalize each word
        name = no_ext.replace("_", " ")

        return " ".join([x.capitalize() for x in name.split(" ")])

    @property
    def user(self):
        existing_user = User()
        if existing_user.load(path=self.path):
            return existing_user


class User(object):
    """
    This object represents a model that holds a single user's data. This model allows you to create new entires or load
    existing ones from a specified path or by name.

    Save a new user
    >>> new_user = User(name="dummy unit test")
    >>> new_user.save()
    True

    Load existing user data:
    >>> user_homer = User(name="homer simpson")
    >>> user_homer.load()
    True

    Test that it is the requested user:
    >>> str(user_homer.name).lower()
    'homer simpson'

    Test for expected name key in data:
    >>> "name" in user_homer.data.keys()
    True

    Test for expected address key in data:
    >>> "address" in user_homer.data.keys()
    True

    Test for expected phone key in data:
    >>> "phone" in user_homer.data.keys()
    True


    """
    def __init__(self, name=None, address=None, phone=None):
        """
        :param str name: Name of user
        :param str phone: A phone number
        :param str address: A street address
        """
        self.name = name
        self.address = address
        self.phone = phone

    @property
    def data(self):
        return {"name": self.name, "address": self.address, "phone":self.phone}

    def set_data(self, **kwargs):
        """
        Set all keys passed as class attributes as long as they are valid
        """
        allowed = ["name", "address", "phone"]
        for key in kwargs.keys():
            if key in allowed:
                setattr(self, key, kwargs.get(key))

        return True

    @staticmethod
    def get_file_safe_name(name):
        """A safe name for the file without spaces"""
        return name.lower().strip().replace(" ", "_")

    def load(self, path=None):
        """
        Force file contents to be loaded and filled into our current User model.

        :param str path: A full path to json file.
        :return: bool

        """
        # load the default db file format
        if path and os.path.splitext(path)[1] == ".json":
            load_path = path
        else:
            load_path = self.get_file_path()

        if not os.path.exists(load_path):
            raise Exception("No user record found for [%s]. Looked for [%s]" % (self.name, load_path))

        return self.load_from_json(load_path)

    def save(self):
        # save in the default file format for our file based db
        return self.save_to_json()

    def validate(self):
        if type(self.name) not in (str, unicode):
            raise Exception("A name is required to save a record and must be a valid string or unicode value.")

        if len(self.name) <= 1:
            raise Exception("Name must have more than 1 letter")

    def get_file_path(self):
        self.validate()
        file_name = self.get_file_safe_name(self.name)
        path = os.path.join(DB_PATH, file_name + ".json")
        return path

    def save_to_json(self):
        path = self.get_file_path()
        with open(path, "w") as output:
            json.dump(self.data, output)

        return True

    def load_from_json(self, path):
        with open(path) as f:
            data = json.load(f)

        return self.set_data(**data)

