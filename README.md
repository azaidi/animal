# Requirements
An installation of python 2.7 and Windows OS

# Installation
Create a new directory and navigate into it:
```
$ mkdir AnimalTest
$ cd AnimalTest
```

Clone this git repo
```
$ git clone https://gitlab.com/azaidi/animal.git
```

Create a python virtual environment for version 2.7
```
$ virtualenv venv
$ venv\Scripts\activate
```

### Install dependencies, or manually install PyYAML 
```
$ pip install -r animal/requirements.txt
```

##### Quick test to see if everything is ok.
```
$ python -m doctest -v animal/users.py
```

***

# Documentation
This is a simple file based user retrieval and storage system. 
Users are stored in simple `.json` files in the `/db` folder of the package to simulate records.

This animal package has three user defined class types to represent user data, all located in `users.py`.
* **User** - Acts as a model for user information. It can save, fill, and load user data.
* **UserRecord** - A preview container that can load a `User` when requested, but mostly 
represents a json file path as a person's name.
* **UserCollection** - A collection of `UserRecord` based on certain search criteria. 
Collections can be manipulated and viewed in different formats.

***

## Python Console
###### From an activated virtual environment invoke a python console that is outside the cloned package directory **/animal**. 
This would be the same directory your `venv` folder was created in.
```
$ python
```


### Access users as a collection

```python
import animal.users as users

users.fetch()

# Result: [Asim Zaidi, ..., Homer Simpson, Lego Aquaman, Lego Batman]
```
This simple command will search the **/db** directory attached to this package and return a `UserCollection` 
all existing users. Users are returned as `UserRecord` instances.

#### Search by name
```python
import animal.users as users

users.fetch("Lego")

# Result: [Lego Aquaman, Lego Batman]
```
Pass in `Lego` as the first argument to get existing records that are `like` the word lego.

#### ...or by exact name
```python
import animal.users as users

users.fetch_first("Lego batman")

# Result: Lego Batman
```

The `fetch_first()` function will search strictly for the requested name and return only 1 result if it is found. 
The returned result will be a `UserRecord`.

### User
#### Add new users
```python
import animal.users as users

new_user = users.User(name="George Lucas", address="123 Skywalker Ranch", phone="999-999-9999")
new_user.save()

# Result: True
```
To create a new user record construct a `User` object with `name`,`address` and `phone` arguments.

#### Update existing user
```python
import animal.users as users

old_user = users.User(name="George Lucas")

# load data matching this user name
old_user.load()

# change something
old_user.phone = "111-111-1111"

# save
old_user.save()

# Result: True
```
To update an existing record, pass the name in first and call the `load()` method. This will fill the User object with 
existing data or throw an exception if no such user file exists. 

Calling `save()` without loading the previous data may override the existing record completely.

### User Collections
User collections are a powerful way to view and convert queried data.
```python
import animal.users as users

collection = users.fetch()
collection.get()

# [
#   {'phone': u'1-604-351-9999', 'name': u'Asim Zaidi', 'address': u'6667 Lloyd Ave'}, 
#   {'phone': u'704-123-4567', 'name': u'Homer Simpson', 'address': u'1 SpringField Ave'},
# ...]
```
The `fetch()` function always returns a `UserCollection`. Using the `get()` method on a user collection returns a python 
array, containing all the data for each record. 

Since the fetch operation is searching the file system for records that match a given name, it does not open each document 
to get all the data. This allows for a fast search with just a preview of the names that match. To get the full data set
for your query always call the `get()` method.
#### In other formats
```python
import animal.users as users

collection = users.fetch()
print collection.formats

# ['yaml', 'json', 'html', 'list', 'text']

```
You can query other supported formats. Currently the following are supported:
1. Yaml
2. Json
3. HTML
4. Python (list)
5. Text

```python
import animal.users as users

collection = users.fetch()
collection.get("text")

# phone: 1-604-351-9999, name: Asim Zaidi, address: 6667 Lloyd Ave, 
# phone: 704-123-4567, name: Homer Simpson, address: 1 SpringField Ave, 
# phone: 123-456-6789, name: Lego Aquaman, address: 123 Water street, 
# phone: 123-456-6789, name: Lego Batman, address: 123 Gotham street, 
```
The example above will return the data in **text** format.

#### Browse collections
```python
import animal.users as users

collection = users.fetch()
collection.browse("html")

```
Collections have a `browse()` method that launches the requested format into a 
web browser using the `webbrowser` module. The **html** format will display an html page with a table.

The **list** format is not supported.

***

## Command Line
Some features are also accessible via command line when accessed from the package directory.

Starting from the cloned directory... `<DIR YOU MADE>/animal`

```
$ cd animal
```

### Get users
```
$ python fetch.py

>>> [Asim Zaidi, Homer Simpson, Lego Aquaman, Lego Batman]
```
#### ...by name
```
$ python fetch.py -n="lego"

>>> [Lego Aquaman, Lego Batman]
```
#### ...or by exact name
```
$ python fetch.py -n="lego batman" -e=1

>>> [Lego Batman]
```
#### Convert to supported formats
```
$ python fetch.py -f="json"

>>> 
[
    {
        "phone": "123-111-1111",
        "name": "Asim Zaidi",
        "address": "6667 Lloyd Ave"
    },
...]
```

### Create users
```
python create.py "Yoda the great" -ad="The swamp" -ph="000-000-0000"

>>> Creating new user Yoda the great
>>> saving....
>>> True
```

## Doctest
This package uses doctests for tests.

Starting from the cloned directory...
```
$ cd animal
```
Run all tests.
```
$ python -m doctest -v users.py
```

If all tests pass you should get an output similar to this:
```
>>>
...
5 items passed all tests:
   8 tests in users.User
   5 tests in users.UserCollection
   1 tests in users.UserRecord
   1 tests in users.fetch
   1 tests in users.fetch_first
16 tests in 32 items.
16 passed and 0 failed.
Test passed.

```

***

## Common pitfalls / rationale
### DB
For this quick exercise I was deciding between storing all records in a single file or in different files. 
Ideally I would store this in a proper database as each user document is small and does not satisfy a need 
to be stored on as a file. For querying and filtering purposes a database would be better. Since using Glob 
was a suggestion I decided to try it out and it made more sense to break each record into a single user 
document to be easy to search. This was a more scalable solution using Glob where as throwing everything 
into one file would have the constant overhead of opening and writing to the same file every time.

The DB directory was only tested on Windows. I am unsure if this will register correctly in an environment 
outside of Windows. More time would be needed to test various environments.

### UserRecord
I decided to create something that would act as an INDEX between the actual data and the query. From past 
experience trying to read every single file on a file system just to find some data inside it becomes a very
 slow process and is not scalable. I decided that the UserRecord class would treat the path to the file as 
 an index column in a database. It would also use the file name as a quick preview of search results as file 
 names are based of the search criteria. This allows for a readable preview of search results and filtering 
 without actually reading the file contents.

### UserCollection
I was inspired to try my own version of this as I have interfaced with collections in advanced frameworks such 
as [Laravel](https://laravel.com/docs/7.x/collections). I found it was much easier to manipulate a dataset into different
outputs once they were loaded into this class. The class can also be extended to support more formats.

It also makes the API easy to read and chain together. Since `fetch()` returns a collection, 
it can easily be chained to control the output. 
```python
users.fetch("lego").get("json")
```
or
```python
users.fetch().browse("html")
```

If I had another try at it, I may have created an additional class that acted as the `RenderClass` or `ViewClass`. 
I would replace the `to_html` and additional methods to instead construct the appropriate class or child class and keep
the class mapping outside of the `UserCollection`.

This would allow developers to inherit the rendering class and just add the code they need to convert our known data
format into a new format and return it. A developer could extend formats by inheriting the parent render class and
registering it without really touching the `UserCollection`.

### Distribution
I was looking for a way to push up all the code and have the end user pull it down to use it easily. At first I 
started packaging this as a PIP installable package. But quickly realized that the `/DB` directory path would end up in 
the site packages folder of a virtual environment the user might create. After running tests it was clear that the API would not 
be able to read or right from that directly correctly. As time was limited, I decided to simplify the install process and
 stay away from a PIP package setup. The best way to use this at this time while keeping the DB directory outside site 
 packages is to clone the repo outside of the `venv`. This method was reproducable at my end from a fresh setup.

In the future I may look into creating and filling the DB directory procedurally after a successful package install. 
The user may have to choose where the database is stored.
